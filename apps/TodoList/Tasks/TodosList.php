<?php

namespace Apps\TodoList\Tasks;

use Core\Libraries\Apps\AbstractTaskManager;
use Core\Libraries\Response\TaskResponse;


class TodosList extends AbstractTaskManager
{
    /**
     * Add new Todo
     * @param $params
     * @return \Core\Libraries\Response\TaskResponse
     * @throws \Exception
     */
    public function create($params){

        $todo = new \Apps\TodoList\Models\TodosList();

        
        if(!$todo){
            $this->response->success = false;
            $this->response->addError(parent::ERROR_SAVING_MODEL,'LANG_TODO_UNKNOWN');
            return $this->response;
        }
        if(!$todo->setTitle($params["title"])){
            $this->response->success = false;
            $this->response->actionErrorField('title');
            $this->response->addError(parent::ERROR_REQUIRED_FIELD, 'TEXT_REQUIRED');
            return $this->response;
        }
        if(!$todo->setDateToComplete($params["date_to_complete"])){
            $this->response->success=false;
            $this->response->actionErrorField('date_to_complete');
            $this->response->addError(parent::ERROR_REQUIRED_FIELD, 'TEXT_REQUIRED');
            return $this->response;
        }
        /** Try Save Model */
        try{
            $success = $todo->save(array(
                'notes' => $params['notes'],
                'user_id' => isset($todo->user_id) ? $todo->user_id : \Beevo::getUser()->getUserId(),
                'status' => $todo::PUBLISHED,
            ));
            $this->response->success = true;
        }
        catch(\Exception $exception){
            $this->response->success = false;
            $this->response->actionFlash($exception, AbstractResponse::FLASH_TYPE_ERROR);
            return $this->response;
        }
        return $this->response;
    }

    /**
     * Update new Todo
     * @param $params
     * @return \Core\Libraries\Response\TaskResponse
     * @throws \Exception
     */
    public function update($params){
        $todo_to_update = new \Apps\TodoList\Models\TodosList();

        if(!$todo_to_update){
            $this->response->success = false;
            $this->response->addError(parent::ERROR_SAVING_MODEL,'LANG_TODO_UNKNOWN');
            return $this->response;
        }
        if(!$todo_to_update->setId($params["id"])){
            $this->response->success = false;
            $this->response->actionErrorField('id');
            $this->response->addError(parent::ERROR_REQUIRED_FIELD, 'TEXT_REQUIRED');
            return $this->response;
        }
        if(!$todo_to_update->setTitle($params["title"])){
            $this->response->success = false;
            $this->response->actionErrorField('title');
            $this->response->addError(parent::ERROR_REQUIRED_FIELD, 'TEXT_REQUIRED');
            return $this->response;
        }
        if(!$todo_to_update->setDateToComplete($params["date_to_complete"])){
            $this->response->success=false;
            $this->response->actionErrorField('date_to_complete');
            $this->response->addError(parent::ERROR_REQUIRED_FIELD, 'TEXT_REQUIRED');
            return $this->response;
        }
        
        /** Try Save Model */
        try{
            $success = $todo_to_update->save(array(
                'notes' => $params["notes"],
                'user_id' => isset($todo->user_id) ? $todo->user_id : \Beevo::getUser()->getUserId(),
                'status' => $todo_to_update::PUBLISHED
            ));
            $this->response->success = true;
        }
        catch(\Exception $exception){
            $this->response->success = false;
            $this->response->actionFlash($exception, AbstractResponse::FLASH_TYPE_ERROR);
            return $this->response;
        }
        return $this->response;
    }

    /**
     * Delete a Todo Item
     * @param $params
     * @return \Core\Libraries\Response\TaskResponse
     * @throws Exception 
     */
    public function delete($params){

        $todo = new \Apps\TodoList\Models\TodosList();
        
        if(!isset($params['todos_ids'])){
            $this->response->success = false;
            $this->response->actionFlash(
                                        'LANG_TODO_DELETE_ERROR',
                                        TaskResponse::FLASH_TYPE_ERROR
                                        );
            return $this->response;
        }

        $total_errors = 0;
        if(is_array($params['todos_ids'])){
            foreach ($params['todos_ids'] as $key => $value) {
                if(!$todo->setId($value)){
                    $this->response->success = false;
                    $this->response->actionErrorField('id');
                    $this->response->addError(parent::ERROR_REQUIRED_FIELD, 'TEXT_REQUIRED');
                    return $this->response;
                }
                $sucess = $todo->delete();
                if(!$sucess){
                    $total_errors++;
                }
            }
            $this->response->success = true;
            return $this->response;
        }
        if(!$todo){
            $this->response->success = false;
            $this->response->addError(parent::ERROR_SAVING_MODEL,'LANG_TODO_UNKNOWN');
            return $this->response;
        }
        if(!$todo->setId($params["id"])){
            $this->response->success = false;
            $this->response->actionErrorField('id');
            $this->response->addError(parent::ERROR_REQUIRED_FIELD, 'TEXT_REQUIRED');
            return $this->response;
        }

        /** Try Save Model */
        try{
            $success = $todo->delete();
            $this->response->success = true;
        }
        catch(\Exception $exception){
            $this->response->success = false;
            $this->response->actionFlash($exception, AbstractResponse::FLASH_TYPE_ERROR);
            return $this->response;
        }
        return $this->response;
    }
    

    /**
     * Publish a Todo Item
     * @param $params
     * @return \Core\Libraries\Response\TaskResponse
     * @throws Exception 
     */
    public function publish($params){

        $todo_to_update = new \Apps\TodoList\Models\TodosList();
        
        if(!isset($params['todos_ids'])){
            $this->response->success = false;
            $this->response->actionFlash(
                                        'LANG_TODO_DELETE_ERROR',
                                        TaskResponse::FLASH_TYPE_ERROR
                                        );
            return $this->response;
        }
        
        $total_errors = 0;
        /** Try Save Model */
        try{
            if(is_array($params['todos_ids'])){
                foreach ($params['todos_ids'] as $key => $value) {
                    $todo = \Apps\TodoList\Models\TodosList::findFirstById($value);
                    if(!$todo_to_update->setId($value)){
                        $this->response->success = false;
                        return $this->response;
                    }
                    $sucess = $todo_to_update->save(array(
                            'title' => $todo->title,
                            'notes' => $todo->notes,
                            'date_to_complete' => $todo->date_to_complete,
                            'user_id' => isset($todo->user_id) ? $todo->user_id : \Beevo::getUser()->getUserId(),
                            'status' => $todo_to_update::PUBLISHED
                    ));
                    if(!$sucess){
                        $total_errors++;
                    }
                }
                $this->response->success = true;
                return $this->response;
            }
            $success = $todo->delete();
            $this->response->success = true;
        }
        catch(\Exception $exception){
            $this->response->success = false;
            $this->response->actionFlash($exception, AbstractResponse::FLASH_TYPE_ERROR);
            return $this->response;
        }
        return $this->response;
    }

    /**
     * UnPublish a Todo Item
     * @param $params
     * @return \Core\Libraries\Response\TaskResponse
     * @throws Exception 
     */
    public function unpublish($params){

        $todo_to_update = new \Apps\TodoList\Models\TodosList();
        
        if(!isset($params['todos_ids'])){
            $this->response->success = false;
            $this->response->actionFlash(
                                        'LANG_TODO_DELETE_ERROR',
                                        TaskResponse::FLASH_TYPE_ERROR
                                        );
            return $this->response;
        }
        
        $total_errors = 0;
        /** Try Save Model */
        try{
            if(is_array($params['todos_ids'])){
                foreach ($params['todos_ids'] as $key => $value) {
                    $todo = \Apps\TodoList\Models\TodosList::findFirstById($value);
                    if(!$todo_to_update->setId($value)){
                        $this->response->success = false;
                        return $this->response;
                    }
                    $sucess = $todo_to_update->save(array(
                            'title' => $todo->title,
                            'notes' => $todo->notes,
                            'date_to_complete' => $todo->date_to_complete,
                            'user_id' => isset($todo->user_id) ? $todo->user_id : \Beevo::getUser()->getUserId(),
                            'status' => $todo_to_update::UNPUBLISHED
                    ));
                    if(!$sucess){
                        $total_errors++;
                    }
                }
                $this->response->success = true;
                return $this->response;
            }
            $success = $todo->delete();
            $this->response->success = true;
        }
        catch(\Exception $exception){
            $this->response->success = false;
            $this->response->actionFlash($exception, AbstractResponse::FLASH_TYPE_ERROR);
            return $this->response;
        }
        return $this->response;
    }
}

?>