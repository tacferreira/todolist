<?php

/**
 * Created by AppManagement
 * User: User
 * Date: 01-01-1970
 * Time: 00:00
 */

namespace Apps\TodoList\Models;

use Core\Libraries\Model\Migrateable;
use Core\Libraries\Model\Model;

/**
 * TodoListConfigurations Model
 * Model to handle the application global configurations
 *
 * @Source('todolist_configurations');
 */
class TodoListConfigurations extends Model implements Migrateable
{
    /**
     * @Primary
     * @Identity
     * @Column(type="string", nullable=false, column="configuration_meta_key")
     * @var string $configuration_meta_key The configuration meta key
     */
    protected $configuration_meta_key;

    /**
     * @Primary
     * @Identity
     * @Column(type="string", nullable=false, column="configuration_key")
     * @var string $configuration_key The configuration key
     */
    protected $configuration_key;

    /**
     * @Column(type="string", nullable=false, column="configuration_value")
     * @var string $configuration_value The configuration value
     */
    protected $configuration_value;

    /**
     * Set the configuration meta key
     * @param string $configuration_meta_key The configuration meta key
     */
    public function setConfigurationMetaKey($configuration_meta_key) {
        $this->configuration_meta_key = $configuration_meta_key;
    }

    /**
     * Set the configuration key
     * @param string $configuration_key The configuration key
     */
    public function setConfigurationKey($configuration_key) {
        $this->configuration_key = $configuration_key;
    }

    /**
     * Set the configuration value
     * @param string $configuration_value The configuration value
     */
    public function setConfigurationValue($configuration_value) {
        $this->configuration_value = $configuration_value;
    }

    /**
     * Get the configuration meta key
     * @return string The configuration metakey
     */
    public function getConfigurationMetaKey() {
        return $this->configuration_meta_key;
    }

    /**
     * Get the configuration key
     * @return string The configuration key
     */
    public function getConfigurationKey() {
        return $this->configuration_key;
    }

    /**
     * Get the configuration value
     * @return string The configuration value
     */
    public function getConfigurationValue() {
        return $this->configuration_value;
    }

} 