<?php
namespace Apps\TodoList\Models;

use Core\Libraries\Behaviors\Status;
use Core\Libraries\Model\IStatus;
use Core\Libraries\Model\Model;
use Core\Libraries\Model\Migrateable;

/**
 * Class TodosList
 * @Source("todos_todos")
 * @package Apps\TodoList\Models
 */
class TodosList extends Model implements Migrateable,IStatus {

    use Status;

    /**
     * @Primary
     * @GUID
     * @Column(type="string", nullable=true, column="id")
     */
    public $id;

    /**
     * @Column(type="string", nullable=true, column="title")
     */
    public $title;

    /**
     * @Column(type="string", nullable=true, column="notes")
     */
    public $notes;

    /**
     * @Column(type="string", nullable=true, column="user_id")
     */
    public $user_id;

    /**
     * @Column(type="date", nullable="true", column="date_to_complete")
    */
    public $date_to_complete;


    /**
     * @param string $id
     * @return Todo
     */
    public function setId($id){
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $title
     * @return Todo
     */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $notes
     * @return Todo
     */
    public function setNotes($notes){
        $this->notes = $notes;
        return $this;
    }

    /**
     * @param string $user_id
     * @return Todo
     */
    public function setUserId($user_id){
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @param string $date
     * @return Todo
     */
    public function setDateToComplete($date_to_complete){
        $this->date_to_complete= $date_to_complete;
        return $this;
    }

}

?>