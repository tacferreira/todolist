EditTodo.prototype = Object.create(Widget.prototype);

function EditTodo(version, app_slug, widget_slug, widget_instance_id, format, widget_params) {
    Widget.call(this, version, app_slug, widget_slug, widget_instance_id, format);
}

EditTodo.prototype.setEventHandlers = function () {
    var self = this;

    var configs = {
        "version": self.version,
        "format": "json",
        "app_slug": self.app_slug,
        "model": "TodosList",
    };

    
    $(this.container + " .btn-update").click(function () {
        configs.task = "update";
        Beevo.submitForm(self.container + ".submit-form", self, configs, "post", function (response){
            if (Beevo.success(response)) {
                window.location.href = '/todo';
            }
        });
    });
};