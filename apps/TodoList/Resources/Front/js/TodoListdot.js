TodoListdot.prototype = Object.create(Widget.prototype);

function TodoListdot(version, app_slug, widget_slug, widget_instance_id, format, widget_params) {
    Widget.call(this, version, app_slug, widget_slug, widget_instance_id, format);
}

TodoListdot.prototype.setEventHandlers = function () {
    var self = this;

    var configs = {
        "version": self.version,
        "format": "json",
        "app_slug": self.app_slug,
        "model": "TodoListdot",
    };

    $(this.container + " .create-btn").click(function () {
        configs.task = "create";
        Beevo.submitForm(self.container + ".submit-form", self, configs, "post", function (response){
            if (Beevo.success(response)) {
                self.reload();
            }
        });
    });

    $(this.container + " .btn-delete").click(function () {
        configs.task = "delete";
        Beevo.submitForm(self.container + ' #' + $(this).attr('formid'), self, configs, "post", function (response){
            if (Beevo.success(response)) {
                window.location.reload();
            }
        });
    });

    $(this.container + " .btn-update").click(function () {
        configs.task = "update";
        Beevo.submitForm(self.container + ' #' + $(this).attr('formid'), self, configs, "post", function (response){
            if (Beevo.success(response)) {
                window.location.reload();
            }
        });
    });

    $(this.container + " .btn-edit").click(function () {
        const noteid = $(this).attr('noteid');
        window.location.href='/todolistdot?view=Edit&id=' + noteid;
    });

    $(this.container + " .btn-edit-update").click(function () {
        configs.task = "updateEdit";
        Beevo.submitForm(self.container + ".submit-form", self, configs, "post", function (response){
            if (Beevo.success(response)) {
                window.location.href='/todolistdot';
            }
        });
    });

    $(this.container + " .note .description").click(function () {
        const elem = $(this);
        const parent = $(this).parent();
        const text_elem = elem.text();
        elem.remove();
        parent.append('<textarea name="note_to_update" class="btn-text form-control">'+text_elem+'</textarea>')
        $(parent).find('.btn-update').removeClass('d-none');
    });
};