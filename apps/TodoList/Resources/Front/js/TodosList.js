TodosList.prototype = Object.create(Widget.prototype);

function TodosList(version, app_slug, widget_slug, widget_instance_id, format, widget_params) {
    Widget.call(this, version, app_slug, widget_slug, widget_instance_id, format);
}

TodosList.prototype.setEventHandlers = function () {
    var self = this;

    var configs = {
        "version": self.version,
        "format": "json",
        "app_slug": self.app_slug,
        "model": "TodosList",
    };
    
    $(this.container + " .create-btn").click(function () {
        configs.task = "create";
        Beevo.submitForm(self.container + ".submit-form", self, configs, "post", function (response){
            if (Beevo.success(response)) {
                self.reload();
            }
        });
    });

    $(this.container + " .btn-edit").click(function () {
        const noteid = $(this).attr('note-id');
        window.location.href='/EditTodo?id=' + noteid;
    });

    $(this.container + " .btn-delete").click(function () {
        const noteid= $(this).attr('noteid');
        configs.task = "delete";
        Beevo.submitForm(self.container + "#item_"+noteid, self, configs, "post", function (response){
            if (Beevo.success(response)) {
                self.reload();
            }
        });
    });
};