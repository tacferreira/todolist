/**
 * Inherit methods from Widget.prototype.
 */
TodoList.prototype = Object.create(WidgetAdmin.prototype);


/**
 * WidgetAdmin constructor
 */
function TodoList(version, app_slug, widget_slug, format, widget_params, admin_manager) {

    // Call the parent constructor
    WidgetAdmin.call(this, version, app_slug, widget_slug, format, widget_params, admin_manager);
}

/**
 * Register the event handlers.
 * Do The Listing
 */
TodoList.prototype.setEventHandlers = function () {
    var self = this;


    var app_name = PageManager.getSingleUriParam('application');
    var model_name = PageManager.getSingleUriParam('model');
    var uri_name_model = '';
    // var uri_params = false;
    if (Helper.isDefined(app_name) && Helper.isDefined(model_name)) {
        // uri_params = true;
        uri_name_model = 'application=' + app_name + '&' + 'model=' + model_name;
    }
}

TodoList.prototype.delete = function (todo_id) {
    var self = this;
    var ids = [];
    var toolbar_action = false;
    if(typeof todo_id === 'object'){
        toolbar_action = true;
        this.validation("delete");
        checkboxes = $(this.container + '#row_checkbox:checked');
        checkboxes.each(function(i,v){
            ids.push($(v).data("row-id"));
        });
    }else{
        ids.push(todo_id);
    }
    var configs = {
        "version" : this.version,
        "format" : "json",
        "app_slug" : this.app_slug,
        "model" : "TodosList",
        "task" : "delete"
    };

    delete self.params['is_api_request_for_filter'];
    self.params.is_first_set_event_handlers = true;
    this.admin_manager.taskRequest(configs, this, 'POST', {}, {todos_ids:ids}, function (response) {
        if(!toolbar_action){
            this.params.is_first_set_event_handlers = true;
            // self.setEventHandlers();
            self.reload();
        } else {
            this.params.is_first_set_event_handlers = false;
            self.setEventHandlers();
        }
    });
}

// Function to Unpublish a Todo Item
TodoList.prototype.unpublish = function (todo_id) {
    var self = this;
    var ids = [];
    var toolbar_action = false;
    if(typeof todo_id === 'object'){
        toolbar_action = true;
        this.validation("unpublish");
        checkboxes = $(this.container + '#row_checkbox:checked');
        checkboxes.each(function(i,v){
            ids.push($(v).data("row-id"));
        });
    }else{
        ids.push(todo_id);
    }
    var configs = {
        "version" : this.version,
        "format" : "json",
        "app_slug" : this.app_slug,
        "model" : "TodosList",
        "task" : "unpublish"
    };

    delete self.params['is_api_request_for_filter'];
    self.params.is_first_set_event_handlers = true;
    this.admin_manager.taskRequest(configs, this, 'POST', {}, {todos_ids:ids}, function (response) {
        if(!toolbar_action){
            this.params.is_first_set_event_handlers = true;
            // self.setEventHandlers();
            self.reload();
        } else {
            this.params.is_first_set_event_handlers = false;
            self.setEventHandlers();
        }
    });
}

// Function to Publish a Todo Item
TodoList.prototype.publish = function (todo_id) {
    var self = this;
    var ids = [];
    var toolbar_action = false;
    if(typeof todo_id === 'object'){
        toolbar_action = true;
        this.validation("publish");
        checkboxes = $(this.container + '#row_checkbox:checked');
        checkboxes.each(function(i,v){
            ids.push($(v).data("row-id"));
        });
    }else{
        ids.push(todo_id);
    }
    var configs = {
        "version" : this.version,
        "format" : "json",
        "app_slug" : this.app_slug,
        "model" : "TodosList",
        "task" : "publish"
    };

    delete self.params['is_api_request_for_filter'];
    self.params.is_first_set_event_handlers = true;
    this.admin_manager.taskRequest(configs, this, 'POST', {}, {todos_ids:ids}, function (response) {
        if(!toolbar_action){
            this.params.is_first_set_event_handlers = true;
            // self.setEventHandlers();
            self.reload();
        } else {
            this.params.is_first_set_event_handlers = false;
            self.setEventHandlers();
        }
    });
}