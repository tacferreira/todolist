/**
 * Inherit methods from Widget.prototype.
 */
TodoListNew.prototype = Object.create(WidgetAdmin.prototype);


/**
 * EditArticle class constructor.
 */
function TodoListNew(version, app_slug, widget_slug, format, widget_params,admin_manager) {
    // Call the parent constructor
    WidgetAdmin.call(this, version, app_slug, widget_slug, format, widget_params,admin_manager);
    this.params.application = (typeof  PageManager.getUriParam("application")[0] !== "undefined") ? PageManager.getUriParam("application")[0] : this.widget_params.application ;
    this.params.model = (typeof PageManager.getUriParam("model")[0] !== "undefined") ? PageManager.getUriParam("model")[0] : this.widget_params.model ;
}


TodoListNew.prototype.setEventHandlers = function () {
    var self = this;

} ;

TodoListNew.prototype.saved = function (data) {
    if(Beevo.success(data)) {
        var todo_id = Beevo.fetchReturnedResult(data).id;
        $(this.container + 'input[name="id"]').val(todo_id);
        this.admin_manager.setWidgetParameters(this,"TodoList",{id: todo_id},"TodoList",todo_id);
    }
};

TodoListNew.prototype.save = function() {
    var configs = {
        "version" : this.version,
        "format" : "json",
        "app_slug" : this.app_slug,
        "model" : "TodosList",
        "task" : "create"
    };

    this.admin_manager.submitForm(this.container + '#admin-form', this, configs, 'POST', function (response){
        if (Beevo.success(response)) {
            window.location.href='/admin/todolist/TodoList?application=TodoList&model=TodosList';
        }
    }) ;
};