{
    "LANG_TODO_STATE":"Status",
    "LANG_TODO_TITLE":"Título",
    "LANG_TODO_USER":"Utilizador",
    "LANG_TODO_ACTIONS":"Ações",
    "LANG_TODO_NO_DATA":"Sem dados",
    "LANG_TODO_EDIT":"EDITAR",
    "LANG_TODO_EDIT_TITLE":"Editar",
    "LANG_TODO_NEW":"Novo Todo"
}