<?php

namespace Apps\TodoList\Widgets\Front\TodosList;

use Core\Libraries\Apps\AbstractWidget;
use Core\Libraries\HTTP\Request;
use Apps\TodoList\Models\TodosList;

class Widget extends AbstractWidget
{

    public $items;

    public function run()
    {
        $this->items = TodosList::find();
        return true;
    }
} 