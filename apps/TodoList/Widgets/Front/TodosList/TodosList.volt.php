
<div class="todo-container p-t-20 p-b-50 m-b-20">
    <form action="" method="post" class="submit-form">
        <div class="row form-group">
            <label for="title">Titulo</label>
            <input class="btn-text form-control" type="text" name="title"/>
        </div>
        <div class="row form-group">
            <label for="date_to_complete">Data</label>
            <input class="btn-text form-control" type="date" name="date_to_complete"/>
        </div>
        <div class="row form-group">
            <label for="notes">Notas</label>
            <textarea class="btn-text form-control" name="notes"></textarea>
        </div>
        <div class="row form-group">
            <button id="btn-create" class="m-5 create-btn btn btn-c-black text-uppercase btn-xs-block" type="button"><i class="fa fa-plus"></i></button>
        </div>
    </form>

    <div class="todos-results form-group col-md-12 p-t-20">
        {% for item in items %}
            <div class="note bg-yellow col-md-3 position-relative" >
                <div class="title color-text-gray">
                    {{item.title}}
                </div>
                <div class="line col-md-12"></div>
                <div class="description color-text-gray">
                    {{item.notes}}
                </div>
                <div class="btn-edit p-absolute" note-id="{{item.id}}">
                        <i class="fa fa-pencil color-text-gray"></i>
                    </div>
                <form id="item_{{item.id}}" class="delete-form" method="post" action="">
                    <input type="hidden" name="id" value="{{item.id}}"/>
                    <div class="btn-delete p-absolute" noteid="{{item.id}}">
                        <i class="fa fa-trash color-text-gray"></i>
                    </div>
                </form>
            </div>     
        {% endfor %}
    </div>
</div>

