<form action="" method="post" class="submit-form">
    <div class="row form-group">
        <label for="title">Titulo</label>    
        <input class="btn-text form-control" type="text" name="title" autocomplete="false" value="{{item.title}}"/>
    </div>
    <div class="row form-group">
        <label for="date_to_complete">Data</label>
        <input class="btn-text form-control" type="date" name="date_to_complete" value="{{item.date_to_complete}}"/>
    </div>
    <div class="row form-group">
        <label for="notes">Notas</label>
        <textarea class="btn-text form-control" name="notes">{{item.notes}}</textarea>
    </div>
    <div class="row form-group">
        <button class="m-5 btn-update btn btn-c-black" type="button"><i class="fa fa-save"></i></button>
    </div>
    <input type="hidden" name="id" value="{{item.id}}"/> 
</form>