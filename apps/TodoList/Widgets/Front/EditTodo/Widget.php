<?php

namespace Apps\TodoList\Widgets\Front\EditTodo;
use Core\Libraries\Apps\AbstractWidget;
use Core\Libraries\HTTP\Request;
use Apps\TodoList\Models\TodosList;

class Widget  extends AbstractWidget
{

    public $item;

    public function run()
    {
        //Get id parameter provided by URL
        $id = Request::fetch('id', '');
        
        if(isset($id)){
            $this->item = TodosList::findFirstById($id);
        }
        return true;
    }
} 