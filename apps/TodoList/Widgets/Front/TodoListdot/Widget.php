<?php

namespace Apps\TodoList\Widgets\Front\TodoListdot;

use Core\Libraries\Apps\AbstractWidget;
use Core\Libraries\HTTP\Request;
use Apps\TodoList\Models\TodosList;

class Widget  extends AbstractWidget
{
    public $items;

    public function run()
    {
        //Get view parameter Provided by URL 
        $view = Request::fetch('view', '');

        //Get id parameter Provided by URL
        $id = Request::fetch('id', '');

        //If the parameters above exists, then change view
        if(isset($view) && isset($id) && $view === 'Edit'){
            $this->view_name='Edit';
            $this->items = TodosList::findFirstById($id);
            return true;
        }

        
        $this->items = TodosList::find();
        return true;
    }
} 