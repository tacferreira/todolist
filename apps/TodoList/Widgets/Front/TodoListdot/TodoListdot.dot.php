
<div class="todo-container p-t-20 p-b-50 m-b-20">
    <form action="" method="post" class="submit-form form-group">
        <div class="row form-group">
            <label for="title">Titulo</label>
            <input class="btn-text form-control" type="text" name="title" autocomplete="false"/>
        </div>
        <div class="row form-group">
            <label for="date_to_complete">Data</label>
            <input class="btn-text form-control" type="date" name="date_to_complete"/>
        </div>
        <div class="row form-group">
            <label for="notes">Notas</label>
            <textarea class="btn-text form-control" name="notes"></textarea>
        </div>
        <button id="btn-create" class="m-5 create-btn btn btn-c-black text-uppercase btn-xs-block form-group" type="button">+</button>
    </form>

    <div class="todos-results col-md-12 p-t-20">
        {{~ it.items :item:i}}
            <div class="note bg-yellow col-md-3 position-relative">
                <form id="item_{{=i}}" class="form" method="post" action="">
                    <div class="title color-text-gray">
                        {{=item.title}}
                    </div>
                    <div class="line col-md-12"></div>
                    <div class="description color-text-gray">
                        {{=item.notes}}
                    </div>
                
                    <input type="hidden" name="id" value="{{=item.id}}"/>
                    <div class="btn-update d-none p-absolute" formid="item_{{=i}}">
                        <i class="fa fa-check color-text-gray"></i>
                    </div>
                    <div class="btn-edit p-absolute" noteid="{{=item.id}}">
                        <i class="fa fa-pencil color-text-gray"></i>
                    </div>
                    <div class="btn-delete p-absolute" id="item_{{=i}}" formid="item_{{=i}}">
                        <i class="fa fa-trash color-text-gray"></i>
                    </div>
                </form>
            </div>     
        {{~}}
    </div>
</div>