<form action="" method="post" class="submit-form">
    <div class="row form-group">
        <label for="title">Titulo</label>    
        <input class="btn-text form-control" type="text" name="title" autocomplete="false" value="{{=it.items.title}}"/>
    </div>
    <div class="row form-group">
        <label for="date_to_complete">Data</label>
        <input class="btn-text form-control" type="date" name="date_to_complete" value="{{=it.items.date_to_complete}}"/>
    </div>
    <div class="row form-group">
        <label for="notes">Notas</label>
        <textarea class="btn-text form-control" name="notes">{{=it.items.notes}}</textarea>
    </div>
    <div class="row form-group">
        <button class="m-5 btn-edit-update btn btn-c-black" type="button"><i class="fa fa-save"></i></button>
    </div>
    <input type="hidden" name="id" value="{{=it.items.id}}"/> 
</form>