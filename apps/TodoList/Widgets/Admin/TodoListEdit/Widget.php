<?php

namespace Apps\TodoList\Widgets\Admin\TodoListEdit;
use Core\Libraries\Apps\AbstractAdminWidget;
use Core\Libraries\HTTP\Request;
use Apps\TodoList\Models\TodosList;

class Widget  extends AbstractAdminWidget
{

    public $item;
    public $model;

    public function run()
    {

        /** @var TodosList $model */
        $model = \Beevo::getModelsFactory()->getModel($this->app_namespace, 'TodosList');

        $id = Request::fetch('id', '');
        
        if(isset($id)){
            $this->item = TodosList::findFirstById($id);
        }
        return true;
    }
} 