
{# if is an api request for filters #}
{% if is_api_request_for_filter %}
    {% for item in paginator.getPaginate().items %}
        {{ partial('table_line.volt.php', {'item':item,'level':level,'application':application,'model':model, 'is_search_active': true, 'c_namespace': c_namespace}) }}
    {% else %}
        <tr>
            <td colspan="7">{{_('LANG_CATEGORIES_NO_DATA')}}</td>
        </tr>
    {% endfor %}
{% else %} {# else for is_ajax_request_for_filters #}


{% if !is_ajax_request %}

<!-- Search bar -->
<div class="panel toolbar_search_advanced">
  <div id="search_toolbar" data-search="search-bar-container" class="search_toolbar">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 p-x-0">
      <!-- Filter published states -->
      <div class="col-xs-6 col-sm-4 col-md-4 p-l-0">
        <select data-selected-text-format="static" data-filter-name="state_search" data-selected-text-format="count" data-field-name="state_search" title="{{_('STATUS')}}" data-style="form-control select-picker-style " multiple data-table-filter="dropdown_top_filter" name="state_search" id="state_search" class="selectpicker m-x-0">
            <option data-hidden="true"></option>
            {% for state in options['states'] %}
              <option data-content="{{state['content']}}" value="{{state['index']}}" {% if state['index'] in filters['state_search'] %}selected="selected"{% endif %}>{{state['plain_text']}}</option>
            {% endfor %}
        </select>
      </div>
      <!-- Filter -->
      <div class="col-xs-6 col-sm-4 col-md-4 p-l-0 p-xs-r-0">
          <select data-filter-name="date_search" disabled data-field-name="page.type" title="{{_('TYPE')}}" data-style="form-control select-picker-style" multiple data-table-filter="dropdown_top_filter" name="type_search" id="type_search" class="selectpicker m-x-0">
              <option data-hidden="true"></option>
          </select>
      </div>
      <!-- Filter -->
      <div class="col-xs-6 col-sm-4 col-md-4 p-l-0 p-sm-r-0 p-xs-r-15 m-xs-t-10 m-sm-b-10">
          <select data-filter-name="state_search" disabled data-field-name="more" title="{{_('MORE')}}" data-style="form-control select-picker-style" data-table-filter="dropdown_top_filter" name="type_search" id="type_search" class="selectpicker m-x-0">
              <option data-hidden="true"></option>
          </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 p-x-0">
      <div class="input-group group_search_tagsinput">
        <input type="text" value="" id='filter_array'  class="filter-lock tag-field" style="display: none;"/>
        <span class="input-group-addon bg-color-transparent text-color-3" name="table_clear_filter">
            <i id="clear_filter_top" name="clear_filter_top" class="fa fa-times clickable" data-table-filter="clear_filter_top"></i>
        </span>
      </div>
    </div>
  </div>
</div>

<!-- Categories table list-->
<div class="panel">
  <table class="table table-responsive style-board">
    <thead>
      <tr>
          <th></th>
          <th>{{ _('LANG_TODO_STATE') }}</th>
          <th>{{ _('LANG_TODO_TITLE') }}</th>
          <th>{{ _('LANG_TODO_USER') }}</th>
          <th class="text-center">{{ _('LANG_TODO_ACTIONS') }}</th>
      </tr>
      <tr class="header-filter-js">
        <th>{{ tag.input('checkbox',['data-row-id':'all', 'id':'select_all_checkbox','wrap-class':'m-a-0 p-a-0']) }}</th>
        <th>
          {{ tag.input('text',['name':'title_search', 'id':'title','class':'form-control','data-table-filter':'text']) }}
        </th>
        <th></th>
        <th>
          {{ tag.input('text',['name':'user_name_search', 'id':'user_name_search','class':'form-control','data-table-filter':'text']) }}
        </th>
        <th class="text-center">
          <i class="fa fa-times clickable" name="table_clear_filter" data-table-filter="clear_filter"></i>
        </th>
      </tr>
    </thead>
    <tbody>
{% endif %}
      {% for item in paginator.getPaginate().items %}
         {{ partial('table_line.volt.php', {'item':item,'level':level,'application':application,'model':model, 'is_search_active': false, 'c_namespace': c_namespace}) }}
       {% else %}
           <tr>
               <td colspan="7">{{_('LANG_TODO_NO_DATA')}}</td>
           </tr>
      {% endfor %}
{% if !is_ajax_request %}
    </tbody>
  </table>
</div>
{{ tag.paginator(['id':paginator_id,'paginator':paginator]) }}
{% endif %}


{% endif %} {# else-> endif for is_ajax_request_for_filters #}
