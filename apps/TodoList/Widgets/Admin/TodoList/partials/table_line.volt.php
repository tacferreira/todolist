<tr id="{{item.id}}" class="level-{{level}} {% if level != 0 %} collapse in {% endif %}">
    <td width="30px">
        {{ tag.input('checkbox',[ 'id':'row_checkbox','wrap-class':'m-a-0 p-a-0','data-row-id':item.id]) }}
      </td>
    <td>
        <button class="btn {% if item.status == 1 %}btn-publish-state{% else %}btn-unpublish-state{% endif %} change-publish-state-js" data-current-state="{{ item.status }}" data-row-id="{{item.id}}">
            <i class="fa {% if item.status == 1 %}fa-check-circle-o{% elseif  item.status == 0 %}fa-ban{% else %}fa-trash{% endif %}" aria-hidden="true"></i>
        </button>
    </td>
    <td>
        {{item.title}}
    </td>
    <td></td>
     <td class="text-center">
        <div class="dropdown clickable">
          <span class="dropdown-toggle" type="button" data-toggle="dropdown">
            <i class="fa fa-cog fa-lg" aria-hidden="true"></i>
          </span>
          <ul class="dropdown-menu pull-right">
            <li><a class="clickable" href="{{edit_link}}?id={{item.id}}&application={{application}}&model={{model}}">
              <!-- <i class="fa fa-pencil"></i> -->
              {{_('LANG_TODO_EDIT')}}</a></li>
            {% if item.status === 1 %}
            <li data-current-state="{{ item.status }}" data-row-id="{{ item.id }}" class="change-publish-state-js"><a
                > {{ _('UNPUBLISH') }} </a></li>
            {% endif %}

            {% if item.status === 0 %}
            <li data-current-state="{{ item.status }}" data-row-id="{{ item.id }}" class="change-publish-state-js"><a
                > {{ _('PUBLISH') }} </a></li>
            {% endif %}
          </ul>
        </div>
    </td>
</tr>
