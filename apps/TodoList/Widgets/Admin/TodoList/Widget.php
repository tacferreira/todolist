<?php

namespace Apps\TodoList\Widgets\Admin\TodoList;
use Core\Libraries\Apps\AbstractAdminWidget;
use Core\Libraries\HTTP\Request;
use Core\Libraries\Uri;
use Core\Libraries\Html\Paginator;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class Widget  extends AbstractAdminWidget
{

    

    public $paginator_id;
    public $current_page;
    public $items_per_page = 20;

    public $edit_link;
    public $paginator;
    public $filters = array();
    public $options = array();
    public $filter_array_to_string = "";
    public $filter_mapping = "";

    public $is_ajax_request = false;
    public $is_api_request_for_filter = false;
    public $level = 0;
    public $application;
    public $model;

    public $items;
    
    public $c_namespace;
    
    public function run()
    {
        
        $this->getWidgetParams();
        
        $conditions = array();
        $binds = array();
        // States -> Publish or Unpublished
        $this->options['states'] =  array(
            array('index' => 1, 'content' => "<i class='fa fa-check-circle-o text-success' aria-hidden='true'></i> ".\Beevo::getLanguage()->_('PUBLISHED'),'plain_text' => \Beevo::getLanguage()->_('PUBLISHED')),
            array('index' => 0, 'content' => "<i class='fa fa-ban text-danger' aria-hidden='true'></i> ".\Beevo::getLanguage()->_('UNPUBLISHED'),'plain_text' => \Beevo::getLanguage()->_('UNPUBLISHED'))
        );
        
        // Filter by publish or unpublish
        $my_filter = array();
        array_push($my_filter,'{ "value":1,"text": ' . \Beevo::getLanguage()->_('PUBLISHED') . ',"type":"state"}');
        array_push($my_filter,'{ "value":0,"text": ' . \Beevo::getLanguage()->_('UNPUBLISHED') . ',"type":"state"}');
        $this->filter_mapping  = json_encode($my_filter);
        //Application who this widget belongs -> Provided by url parameter
        $this->application = Request::fetch('application', '');
        if(isset($this->uri_params['application'])){
            $this->application = $this->uri_params['application'];
        }
        
        //Model who this widget belongs -> Provided by url parameter
        $this->model = Request::fetch('model', '');
        if(isset($this->uri_params['model'])){
            $this->model = $this->uri_params['model'];
        }
        
        
        $conditions[] = ' app_namespace = :app_namespace: ';
        $conditions[] = ' model_namespace = :model_namespace: ';
        $binds['app_namespace'] = $this->application;
        $binds['model_namespace'] = $this->model;
        
        
        //condition to build model conditions for normal table, apiRequest showChilds and apiRequest filter
        if($this->is_api_request_for_filter){ // apiRequest -> filtered items
            
            //fetch filters
            $title_search = Request::fetch('title_search', '');
            $state_search = Request::fetch('state_search',null);
            
            //filter by name
            if(isset($title_search)){
                if($title_search != ''){
                    $conditions[] = 'title LIKE :title_search:';
                    $binds['title'] = '%'.$title_search.'%';
                }
            }
            
            //filter by states
            if(isset($state_search) && $state_search != 'null'){
                $state_search = explode(",", $state_search);
                if(!empty($state_search)){
                    $conditions[] = 'status IN ({states:array})';
                    $binds['states'] = array_values($state_search);
                }
            }
            
        }
        
        $final_condition = array('conditions' => implode(' AND ', $conditions), 'bind' => $binds,'order' => 'ordering');
        if($this->is_api_request_for_filter) $final_condition['limit'] = $this->items_per_page;
        
        /** @var TodosList $model */
        $model = \Beevo::getModelsFactory()->getModel($this->app_namespace, 'TodosList');
        $this->items = $model::find();

        $this->paginator = new PaginatorModel(
            array(
                "data" => $this->items,
                "limit" => $this->items_per_page,
                "page" => $this->current_page
            )
        );

        $this->edit_link = Uri::linkToWidgetAdminPage('TodoList','TodoListEdit');
        return true;
    }

    private function getWidgetParams(){
        $this->paginator_id = "todos-list";
        $this->current_page = Request::fetch('current_page_'.$this->paginator_id, 1);
        $this->items_per_page = Request::fetch('items_per_page_'.$this->paginator_id, 20);
        $this->filters = Request::fetch('filters', array());


        if(!is_array($this->filters)) $this->filters = array();
        if(!isset($this->filters["state_search"])) $this->filters["state_search"] = array();
        if(!is_array($this->filters["state_search"])) $this->filters["state_search"] = array();

        $this->current_page = Request::fetch( 'page', '1');
        $this->search = Request::fetch( 'search', '');

        //check if is an request from filter events
        $this->is_api_request_for_filter = Request::fetch("is_api_request_for_filter", false);
        if($this->is_api_request_for_filter === 'false') $this->is_api_request_for_filter = false;

    }
} 