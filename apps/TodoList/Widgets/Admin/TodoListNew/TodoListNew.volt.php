<form action="" id="admin-form" method="post" class="submit-form">
    <div class="row form-group">
        <label for="title">Titulo</label>    
        <input class="btn-text form-control" type="text" name="title" autocomplete="false" value=""/>
    </div>
    <div class="row form-group">
        <label for="date_to_complete">Data</label>
        <input class="btn-text form-control" type="date" name="date_to_complete" value=""/>
    </div>
    <div class="row form-group">
        <label for="notes">Notas</label>
        <textarea class="btn-text form-control" name="notes"></textarea>
    </div>
</form>