CREATE TABLE IF NOT EXISTS `${pre_core}todolist_configurations` (configuration_meta_key varchar(100) NOT NULL,configuration_key varchar(91) NOT NULL,configuration_value mediumtext NOT NULL,PRIMARY KEY (`configuration_meta_key`,`configuration_key`));
CREATE TABLE IF NOT EXISTS `${pre_core}todolist_todos` (
 `id` CHAR(36) NOT NULL,
 `title` VARCHAR(255) NOT NULL,
 `notes` TEXT NULL,
 `user_id` CHAR(36) NOT NULL,
 `date_to_complete` DATE NULL,
 `status` INT NOT NULL DEFAULT 0,
PRIMARY KEY (`id`));